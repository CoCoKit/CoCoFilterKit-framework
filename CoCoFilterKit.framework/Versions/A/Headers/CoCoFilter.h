//
//  CoCoFilter.h
//  CoCoFilterKit
//
//  Created by 陈明 on 2017/10/31.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import "CoCoBaseFilter.h"


@interface CoCoFilter : CoCoBaseFilter
@property (nonatomic, strong) NSString *eniv;
@property (nonatomic, strong) NSString *vertexShader;
@property (nonatomic, strong) NSString *fragmentShader;
@property (nonatomic, strong) NSMutableArray *texturePaths;
@property (nonatomic, assign) NSInteger inputCount;
@property (nonatomic, strong) id output;
@property (nonatomic, strong) UIImage *iconImage;
@end
