//
//  GPUImageScaleFilter.h
//  CoCoFilterKit
//
//  Created by 陈明 on 2017/12/7.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <CoCoGPUImage/GPUImage.h>

@interface GPUImageScaleFilter : GPUImageFilter

@property (nonatomic) CGFloat scale;
@property (nonatomic, strong) NSArray *scaleRange;
@property (nonatomic, assign) int frameIndex;

@end
