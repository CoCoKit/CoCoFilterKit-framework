//
//  CoCoFilterGroup.h
//  CoCoFilterKit
//
//  Created by 陈明 on 2017/10/31.
//  Copyright © 2017年 CoCo. All rights reserved.
//



#import "CoCoFilter.h"

@interface CoCoFilterGroup : NSObject
@property (nonatomic, strong) id filterGroup;
- (instancetype)initWithCoCoFilter:(CoCoFilter *)filter;

- (void)setMixInfo:(CGFloat)newValue;
- (void)setScaleInfo:(CGFloat)newValue;
- (void)processTexture;
- (NSString *)name;
@end
