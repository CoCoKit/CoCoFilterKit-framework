//
//  CoCoFilterKit.h
//  CoCoFilterKit
//
//  Created by 陈明 on 2017/10/31.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoCoBaseFilter.h"
#import "CoCoFilter.h"
#import "CoCoFilterManager.h"
// #import "GPUImageBeautyFilter.h"
// #import "GPUImageEmptyFilter.h"
// #import "SFGPUImageHighPassSkinSmoothingFilter.h"
// #import "SFGPUImageStillImageHighPassFilter.h"

// ! Project version number for CoCoFilterKit.
FOUNDATION_EXPORT double CoCoFilterKitVersionNumber;

// ! Project version string for CoCoFilterKit.
FOUNDATION_EXPORT const unsigned char CoCoFilterKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoCoFilterKit/PublicHeader.h>
