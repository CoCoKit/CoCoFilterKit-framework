//
//  CoCoFilterManager.h
//  CoCoFilterKit
//
//  Created by 陈明 on 2017/10/31.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoCoFilter.h"
#import "CoCoFilterGroup.h"

@interface CoCoFilterManager : NSObject
+ (void)PreInit;
+ (void)runManager;
+ (NSArray <CoCoFilter *> *)BuiltInFilters;

+ (NSString *)decryptedWithString:(NSString *)base64String version:(NSString *)version;
@end
