//
//  CoCoBaseFilter.h
//  CoCoFilterKit
//
//  Created by 陈明 on 2017/10/31.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CoCoFilterTypeCustom = 0,
    CoCoFilterTypeBuiltIn = 1,
} CoCoFilterType;

@interface CoCoBaseFilter : NSObject
@property (nonatomic, strong) NSString *fid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CoCoFilterType type;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *iconPath;
@property (nonatomic, strong) NSDictionary *params;

- (instancetype)initWithFilterPath:(NSString *)path;
@end
