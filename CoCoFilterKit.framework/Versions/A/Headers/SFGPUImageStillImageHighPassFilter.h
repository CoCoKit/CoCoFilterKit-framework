//
//  YUGPUImageHighPassStillImageFilter.h
//  Pods
//
//  Created by YuAo on 1/24/16.
//
//

#import <CoCoGPUImage/GPUImage.h>

@interface SFGPUImageStillImageHighPassFilter : GPUImageFilterGroup

@property (nonatomic) CGFloat radiusInPixels;

@end
