//
//  YUGPUImageHighPassSkinSmoothing
//
//  Created by YuAo on 1/24/16.
//
//

#import <CoCoGPUImage/GPUImage.h>

typedef NS_ENUM (NSInteger, SFGPUImageHighPassSkinSmoothingRadiusUnit) {
    YUGPUImageHighPassSkinSmoothingRadiusUnitPixel                = 1,
    YUGPUImageHighPassSkinSmoothingRadiusUnitFractionOfImageWidth = 2
};

@interface SFGPUImageHighPassSkinSmoothingRadius : NSObject <NSCopying, NSSecureCoding>

@property (nonatomic, readonly) CGFloat value;
@property (nonatomic, readonly) SFGPUImageHighPassSkinSmoothingRadiusUnit unit;

- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)radiusInPixels:(CGFloat)pixels;
+ (instancetype)radiusAsFractionOfImageWidth:(CGFloat)fraction;

@end

@interface SFGPUImageHighPassSkinSmoothingFilter : GPUImageFilterGroup

@property (nonatomic) CGFloat amount;

@property (nonatomic, copy) NSArray<NSValue *> *controlPoints; // value of Point

@property (nonatomic, copy) SFGPUImageHighPassSkinSmoothingRadius *radius;

@property (nonatomic) CGFloat sharpnessFactor;

@end
